# Peekaboo Connect Cordova

## Installation Steps

- Clone peekaboo-connect-cordova project using the credential provided in partner configurations

```bash
git clone https://gitlab.com/fetchsky/peekaboo-connect-cordova.git
```

- Install Plugin in your cordova project

```bash
cordova plugin add path/to/your/plugin/repo
```

### Android

- Please make sure your app has `MainApplication.java` file. Also make it extend from `com.peekaboosdk.MainApplication`

#### Update AndroidManifest.xml

- Please add `xmlns:tools="http://schemas.android.com/tools"` on `manifest` tag.
- Please add these attributes on application tag in `AndroidManifest.xml`
  - `android:name=".MainApplication"`
    <!-- - `android:theme="@android:style/Theme.NoTitleBar.Fullscreen"` -->
- Please add [launch_screen.xml](layout/launch_screen.xml) in android resources, it will show up while SDK is loading.

#### Add Peekaboo theme in style.xml

```xml
   <style name="PeekabooTheme" parent="Theme.AppCompat.Light.DarkActionBar">
        <item name="windowActionBar">false</item>
        <item name="windowNoTitle">true</item>
        <item name="android:windowTranslucentNavigation">true</item>
        <item name="android:windowBackground">@color/white</item>
    </style>
```

#### Update app/build.gradle with following peices

```groovy
  ...
  android {
    ...
    compileSdkVersion 29
    dexOptions {
        jumboMode = true
        javaMaxHeapSize "8g"
    }
    compileOptions {
        sourceCompatibility 1.8
        targetCompatibility 1.8
    }
    packagingOptions {
        pickFirst '**/armeabi-v7a/libc++_shared.so'
        pickFirst '**/x86/libc++_shared.so'
        pickFirst '**/arm64-v8a/libc++_shared.so'
        pickFirst '**/x86_64/libc++_shared.so'
        pickFirst '**/x86/libjsc.so'
        pickFirst '**/armeabi-v7a/libjsc.so'
    }
    ...
  }
  ...
```

#### Proguard Rules

Please add following rules in your `proguard-rules.pro` file

```text
# React Native

# Keep our interfaces so they can be used by other ProGuard rules.
# See http://sourceforge.net/p/proguard/bugs/466/
-keep,allowobfuscation @interface com.facebook.proguard.annotations.DoNotStrip
-keep,allowobfuscation @interface com.facebook.proguard.annotations.KeepGettersAndSetters
-keep,allowobfuscation @interface com.facebook.common.internal.DoNotStrip
-keep class com.facebook.react.cxxbridge.ModuleRegistryHolder { *; }
-keep class com.facebook.react.cxxbridge.CatalystInstanceImpl { *; }
-keep class com.facebook.react.cxxbridge.JavaScriptExecutor { *; }
-keep class com.facebook.react.bridge.queue.NativeRunnable { *; }
-keep class com.facebook.react.bridge.ExecutorToken { *; }
-keep class com.facebook.react.bridge.ReadableType { *; }
-keep class com.facebook.react.bridge.* { *; }
-keep class com.facebook.react.uimanager.* { *; }
-keep class org.reactnative.camera.* { *; }



# Do not strip any method/class that is annotated with @DoNotStrip
-keep @com.facebook.proguard.annotations.DoNotStrip class *
-keep @com.facebook.common.internal.DoNotStrip class *
-keepclassmembers class * {
    @com.facebook.proguard.annotations.DoNotStrip *;
    @com.facebook.common.internal.DoNotStrip *;
}

-keepclassmembers @com.facebook.proguard.annotations.KeepGettersAndSetters class * {
  void set*(***);
  *** get*();
}

-keep class * extends com.facebook.react.bridge.JavaScriptModule { *; }
-keep class * extends com.facebook.react.bridge.NativeModule { *; }
-keepclassmembers,includedescriptorclasses class * { native <methods>; }
-keepclassmembers class *  { @com.facebook.react.uimanager.UIProp <fields>; }
-keepclassmembers class *  { @com.facebook.react.uimanager.annotations.ReactProp <methods>; }
-keepclassmembers class *  { @com.facebook.react.uimanager.annotations.ReactPropGroup <methods>; }

-dontwarn com.facebook.react.**

# TextLayoutBuilder uses a non-public Android constructor within StaticLayout.
# See libs/proxy/src/main/java/com/facebook/fbui/textlayoutbuilder/proxy for details.
-dontwarn android.text.StaticLayout

# okhttp
-keepattributes Signature
-keepattributes *Annotation*
-keep class okhttp3.** { *; }
-keep interface okhttp3.** { *; }
-dontwarn okhttp3.**


# okio
-keep class sun.misc.Unsafe { *; }
-dontwarn java.nio.file.*
-dontwarn org.codehaus.mojo.animal_sniffer.IgnoreJRERequirement
-dontwarn okio.**

-keepattributes Annotation
-keep class com.facebook.react.cxxbridge
-keep class com.facebook.react.*

-dontwarn com.facebook.react.**
-keep class com.facebook.react.**
-keep interface com.facebook.react.**

-keep class android.graphics.**

-dontwarn com.facebook.fbui.**
-dontnote com.facebook.fbui.**
-keep class com.facebook.fbui.* { *; }

# retrofit2
-dontwarn retrofit2.**
-keep class retrofit2.* { *; }
-keepattributes Signature
-keepattributes Exceptions
-keepclasseswithmembers class * {
    @retrofit2.http.* <methods>;
}
-keep class com.google.android.gms.** { *; }
-dontwarn com.google.android.gms.**
```

Please add following content in res/raw/keep.xml for keeping images files intact during gradle shrink resources. Read more about [this here](https://developer.android.com/studio/build/shrink-code#keep-resources)

```
<?xml version="1.0" encoding="utf-8"?>
<resources xmlns:tools="http://schemas.android.com/tools"
    tools:keep="@drawable/**" />
```

#### Note Peekaboo Connect Android uses google maps, please google maps api key in your app's manifest

### iOS

- Initialize Peekaboo instance in your AppDelegate.m

```ObjC
#import "AppDelegate.h"
#import "MainViewController.h"
#import <PeekabooConnect/PeekabooConnect.h> // Add this line for peekaboo

@implementation AppDelegate

- (BOOL)application:(UIApplication*)application didFinishLaunchingWithOptions:(NSDictionary*)launchOptions
{
    self.viewController = [[MainViewController alloc] init];
    initializePeekaboo(); // Add this line for peekaboo
    return [super application:application didFinishLaunchingWithOptions:launchOptions];
}

@end
```

- Make sure PeekabooConnect.framework is appearing in Embedded Binaries in your project's general settings tab and is _NOT_ included in Linked Libraries.
- For uploading on iTunes please follow [this guide](https://gitlab.com/fetchsky/peekaboo-connect-sdk/tree/master/iOS#uploading-on-itunes)
- Please add description permissions for Camera & Location when in use.

### Javascript Implementation

- Use this method when you want to initialize peekaboo connect

```javascript
  initPeekabooGuru() {
    window['peekaboo'].init({
      pkbc: '', // this will be communicated to partner by peekaboo team
      environment: 'production', // stage, beta, production
      type: 'deals',  // deals, locator, stores, rewards
      country: 'Pakistan',
      initialRoute: "/",
    });
  }
```
