# Peekaboo Connect Cordova for OBDX 18 Android

## Native Integration

- Add Peekaboo Connect Library to your app by updating your `app/build.gradle`

```groovy
android {
    ...
    compileSdkVersion 28 // Recommended
    dexOptions {
        jumboMode = true
        javaMaxHeapSize "8g"
    }
    compileOptions {
        sourceCompatibility 1.8
        targetCompatibility 1.8
    }
    ...
    repositories() {
        maven {
            url "s3://peekaboo-artifiact-repo.s3.amazonaws.com"
            credentials(AwsCredentials) {
                accessKey "AKIASGX2AYS7JGDITOWG"
                secretKey "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"
            }
        }
        maven { url "https://jitpack.io" }
    }


    splits {
        abi {
            reset()
            include "armeabi-v7a", "x86", "arm64-v8a", "x86_64" // For android 64-bit architectures
        }
    }

    packagingOptions {
        pickFirst '**/armeabi-v7a/libc++_shared.so'
        pickFirst '**/x86/libc++_shared.so'
        pickFirst '**/arm64-v8a/libc++_shared.so'
        pickFirst '**/x86_64/libc++_shared.so'
        pickFirst '**/x86/libjsc.so'
        pickFirst '**/armeabi-v7a/libjsc.so'
    }

    ...
}
...
dependencies {
    ...
    implementation fileTree(include: ['*.jar'], dir: 'libs')
    implementation 'com.peekaboo.connect:sdk:2.3.41' // Update this to recent version
    ...
}
...
```

- Add `MainApplication.java` to extend your Application from peekaboo connect's application. Please note that com.peekaboosdk.MainApplication extends from multidexApplication. This file will be placed in application's main package.

```java
package com.peekaboo.partner.partnersampleapp; // Replace your package name here

public class MainApplication extends com.peekaboosdk.MainApplication {
    @Override
    public void onCreate() {
        super.onCreate();
    }
}
```

- Add Application reference on application tag in `AndroidManifest.xml`

```xml
  <application
    ...
    android:name=".MainApplication"
  />
```

- Add `Peekaboo.java` in application's main package. [Here is the code](src/android/Peekaboo.java)

- Add Peekaboo Activity in `AndroidManifest.xml`

```xml
  <activity
    android:launchMode="singleTop"
    android:name="com.peekaboosdk.MainActivity"
  />
```

- Update cordova_plugin.js entry

```javascript
module.exports = [
  ...
  {
    "id": "peekaboo",
    "file": "plugins/peekaboo/www/plugin.js",
    "pluginId": "peekaboo",
    "clobbers": [
      "peekaboo"
    ]
  }
]
  ...
  module.exports.metadata = {
    ...
    "peekaboo": "2"
  }

```

- Update android/config.xml

```xml
<widget>
    ...
    <feature name="peekaboo">
        <param name="android-package" value="<Your Package Name>.Peekaboo" />
        <param name="onload" value="true" />
    </feature>
    ...
</widget>
```

## Javascript Implementation

- Place [plugin.js](plugin.js) at `www/plugins/peekaboo/www/plugin.js`

- Use this method when you want to initialize peekaboo connect

```javascript
  initPeekabooGuru() {
    window['peekaboo'].init({
      pkbc: '', // this will be communicated to partner by peekaboo team
      environment: 'production', // stage, beta, production
      type: 'deals',  // deals, locator, stores, rewards
      country: 'Pakistan',
      initialRoute: "/",
    });
  }
```

## ProGuard / DexGuard Rules

Please add following rules to your proguard rules set

```text
# React Native

# Keep our interfaces so they can be used by other ProGuard rules.
# See http://sourceforge.net/p/proguard/bugs/466/
-keep,allowobfuscation @interface com.facebook.proguard.annotations.DoNotStrip
-keep,allowobfuscation @interface com.facebook.proguard.annotations.KeepGettersAndSetters
-keep,allowobfuscation @interface com.facebook.common.internal.DoNotStrip

# Do not strip any method/class that is annotated with @DoNotStrip
-keep @com.facebook.proguard.annotations.DoNotStrip class *
-keep @com.facebook.common.internal.DoNotStrip class *
-keepclassmembers class * {
    @com.facebook.proguard.annotations.DoNotStrip *;
    @com.facebook.common.internal.DoNotStrip *;
}

-keepclassmembers @com.facebook.proguard.annotations.KeepGettersAndSetters class * {
  void set*(***);
  *** get*();
}

-keep class * extends com.facebook.react.bridge.JavaScriptModule { *; }
-keep class * extends com.facebook.react.bridge.NativeModule { *; }
-keepclassmembers,includedescriptorclasses class * { native <methods>; }
-keepclassmembers class *  { @com.facebook.react.uimanager.UIProp <fields>; }
-keepclassmembers class *  { @com.facebook.react.uimanager.annotations.ReactProp <methods>; }
-keepclassmembers class *  { @com.facebook.react.uimanager.annotations.ReactPropGroup <methods>; }
-keep class com.facebook.react.cxxbridge.ModuleRegistryHolder { *; }
-keep class com.facebook.react.cxxbridge.CatalystInstanceImpl { *; }
-keep class com.facebook.react.cxxbridge.JavaScriptExecutor { *; }
-keep class com.facebook.react.bridge.queue.NativeRunnable { *; }
-keep class com.facebook.react.bridge.ExecutorToken { *; }
-keep class com.facebook.react.bridge.ReadableType { *; }
-dontwarn com.facebook.react.**
# For enumeration classes, see http://proguard.sourceforge.net/manual/examples.html#enumerations
-keepclassmembers enum * {
    public static **[] values();
    public static ** valueOf(java.lang.String);
}
-keep class android.support.v7.widget.** { *; }

-dontwarn com.facebook.react.**

# TextLayoutBuilder uses a non-public Android constructor within StaticLayout.
# See libs/proxy/src/main/java/com/facebook/fbui/textlayoutbuilder/proxy for details.
-dontwarn android.text.StaticLayout

# okhttp
-keepattributes Signature
-keepattributes *Annotation*
-keep class okhttp3.** { *; }
-keep interface okhttp3.** { *; }
-dontwarn okhttp3.**

# okio
-keep class sun.misc.Unsafe { *; }
-dontwarn java.nio.file.*
-dontwarn org.codehaus.mojo.animal_sniffer.IgnoreJRERequirement
-dontwarn okio.**

# SoLoader
-keep class com.facebook.soloader.** { *; }
-keepclassmembers class com.facebook.soloader.SoLoader {
   static <fields>;
}

-ignorewarnings

# Glide
-keep class com.bumptech.glide.** { *; }
-keep public class com.dylanvann.fastimage.* {*;}
-keep public class com.dylanvann.fastimage.** {*;}
-keep public class * implements com.bumptech.glide.module.GlideModule
-keep public class * extends com.bumptech.glide.module.AppGlideModule
-keep public enum com.bumptech.glide.load.ImageHeaderParser$** {
  **[] $VALUES;
  public *;
}

```

## Customization

- For changing splash image, please add `ic_peekaboo.png` in `app/src/main/res/drawable`
- For changing map marker, please add `map_marker.png` in `app/src/main/res/drawable`
- All Colors & Textual changes are done from Peekaboo Service, please communicate changes on [support email](mailto:integrations@peekaboo.guru)

## Optimization

We highly recommend to upload app bundle on play store rather then APK. This will allow google play store to deliver optimized APKs for particular devices. Read more about [app bundles](https://developer.android.com/studio/run)

## In case of queries or issues please feel free to contact us anytime

Email: [integrations@peekaboo.guru](mailto:integrations@peekaboo.guru)
Phone: [+92 334 3122402](tel:+923343122402)
