# Peekaboo Connect Cordova for OBDX 18 iOS

## Native Integrations

- Add following files to your iOS project.
  - [Peekaboo Connect Framework](libs/PeekabooConnect.Framework)
  - [Peekaboo Connect Bundle](libs/PeekabooConnect.bundle)
  - [Peekaboo Header](src/ios/peekaboo.h)
  - [Peekaboo Class](src/ios/peekaboo.m)

- Add Framework in Embedded Framework's.

- Add bundle in project resources

- Update ios/<ProjectName>/config.xml

```xml
<widget>
    ...
    <feature name="peekaboo">
        <param name="ios-package" value="peekaboo" />
    </feature>
    ...
</widget>
```

## Javascript Implementation

- Place [plugin.js](plugin.js) at `www/plugins/peekaboo/www/plugin.js`

- Use this method when you want to initialize peekaboo connect

```javascript
  initPeekabooGuru() {
    window['peekaboo'].init({
      pkbc: '', // this will be communicated to partner by peekaboo team
      environment: 'production', // stage, beta, production
      type: 'deals',  // deals, locator, stores, rewards
      country: 'Pakistan',
      initialRoute: "/",
    });
  }
```

## Customization

- All Colors & Textual changes are done from Peekaboo Service, please communicate changes on [support email](mailto:integrations@peekaboo.guru)

## Optimization

We highly recommend to upload app bundle on play store rather then APK. This will allow google play store to deliver optimized APKs for particular devices. Read more about [app bundles](https://developer.android.com/studio/run)

## In case of queries or issues please feel free to contact us anytime

Email: [integrations@peekaboo.guru](mailto:integrations@peekaboo.guru)
Phone: [+92 334 3122402](tel:+923343122402)
