/********* peekaboo.m Cordova Plugin Implementation *******/
#import "peekaboo.h"
#import <Cordova/CDV.h>
#import <PeekabooConnect/PeekabooConnect.h>

@interface peekaboo : CDVPlugin {
    PeekabooConnect *peekabooConnect;
}

- (void)init:(CDVInvokedUrlCommand*)command;

@end

@implementation peekaboo

- (void)init:(CDVInvokedUrlCommand*)command{
    // Object of configs
    NSDictionary *options = [command.arguments objectAtIndex:0];
    if (!peekabooConnect) {
        peekabooConnect = [[PeekabooConnect alloc] init];
        [peekabooConnect initializePeekaboo];
    }
    NSDictionary *params = @{
                             @"environment" : [options objectForKey:@"environment"],
                             @"pkbc" : [options objectForKey:@"pkbc"],
                             @"initialRoute": [options objectForKey:@"initialRoute"],
                             @"type": [options objectForKey:@"type"],
                             @"disableGoogleAnalytics": @YES,
                             @"country": [options objectForKey:@"country"],
                             @"fcmInstanceId": [options objectForKey:@"fcmInstanceId"],
                             };
    [self.viewController presentViewController:[peekabooConnect getPeekabooUIViewController:params] animated:YES completion:nil];
}

@end
